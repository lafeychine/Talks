;;; package --- Summary

;;; Commentary:
;;; Setup Emacs to build Talks made with Org-Mode.

;;; Code:
(setq org-latex-compiler "xelatex")

(setq org-latex-default-packages-alist
      '(("normalem" "ulem" t nil)
        ("" "amsmath" t nil)
        ("" "amssymb" t nil)
        ("" "hyperref" t nil)
        ("AUTO" "babel" t nil)))

(setq org-latex-classes
   '(("slides" "\\documentclass{slides}"
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(provide 'init.el)
;;; init.el ends here
