\NeedsTeXFormat{LaTeX2e}
\RequirePackage{expl3}
\ProvidesExplClass{slides}{2022-01-01}{1.0}{Slides beamer class}

% Base class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions\relax

\LoadClass[aspectratio=169, presentation]{beamer}

% Dependencies packages
\RequirePackage{enumitem}
\RequirePackage{ragged2e}
\RequirePackage{hyperref}
\RequirePackage[normalem]{ulem}
\RequirePackage[mathrm=sym]{unicode-math}
\RequirePackage{xstring}

% Configuration
\usetheme{metropolis}
\metroset{sectionpage=simple, block=fill}
\beamerdefaultoverlayspecification{<+->}

\setmathfont{Latin ~ Modern ~ Math}

\def\defaultbeamer{\beamer@currentmode}

\NewDocumentCommand{\changemode}{m} {
  \IfEq{#1}{default}{
    \gdef\beamer@currentmode{\defaultbeamer}
  }{
    \gdef\beamer@currentmode{#1}
  }
}

\setlist[itemize, 1]{before=\justify, label=, left=0pt}
\setlist[itemize]{label=\textbullet}
