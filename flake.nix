{
  description = "Lafeychine's Talks";

  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, devshell, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ devshell.overlay ];
        };
        fonts = pkgs.makeFontsConf {
          fontDirectories = with pkgs; [ lmodern fira fira-mono ];
        };
      in {
        devShell = pkgs.devshell.mkShell {
          name = "Talks";
          env = [{
            name = "TEXINPUTS";
            value = builtins.toString ./. + "/package//:";
          }];
          packages = with pkgs; [
            emacs-nox
            inkscape
            gnuplot
            python3Packages.pygments
            texlive.combined.scheme-full
          ];
        };
      });
}
